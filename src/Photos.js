import React from 'react';
import PropTypes from 'prop-types';

const Photo = ({ id, title, url }) => {
  return (
    <div className="photo">
      <img src={url} alt={title} />
      <p>{title}</p>
    </div>
  );
};

Photo.propTypes = {
  id: PropTypes.number.isRequired,
  title: PropTypes.string.isRequired,
  url: PropTypes.string.isRequired,
};

const Photos = ({ photos }) => {
  return (
    <div className="photos">
      {photos.map((photo) => (
        <Photo key={photo.id} {...photo} />
      ))}
    </div>
  );
};

Photos.propTypes = {
  photos: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      title: PropTypes.string.isRequired,
      url: PropTypes.string.isRequired,
    })
  ).isRequired,
};

export default Photos;